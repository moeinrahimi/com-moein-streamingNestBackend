const express = require('express')
let config = require('_/config')
const userHelper = require('_/helpers/db/userHelper')
const db = require('_/models')
const settingHelper = require('_/helpers/db/settingHelper')
const streamerRevenueHelper = require('_/helpers/db/streamerRevenueHelper')
const zarinPal = require('_/helpers/zarinPal')
const idpay = require('_/helpers/idpay')
const paymentHelper = require('_/helpers/payment')
const goalHelper = require("_/helpers/db/goalHelper")
const alert = require('_/helpers/alert')
const moment = require('moment')

let donateRouter = express.Router();
donateRouter.post(`/:userId`, createPayment)
donateRouter.get(`/callback`, paymentCallback)
donateRouter.post(`/idpay/callback`, idPayCallback)
donateRouter.get(`/getTitledDonates`, getTitledDonates)

async function createPayment(req, res,next) {
  let { donationAmount, donorMessage,donorName } = req.body
  let { userId } = req.params
  if(!donationAmount)  return res.status(400).json({ success: false,message_id:2, message:'مبلغ حمایت مشخص نشده است' })
  try {
    let user = await userHelper.findUserWhere({ id: userId })
    if (!user) return res.status(400).json({ success: false, message: 'چنین کاربری وجود ندارد', message_id: 3 })
    let payment = await settingHelper.activeGateway(userId)
    if(payment.code) return res.status(payment.code).json(payment.res)
    let provider = 'zarinpal'
    if(payment.key == 'BANK_idpay') provider = 'idpay'
    let revenueBody = {
      name:donorName,
      message:donorMessage,
      userId:userId,
      transactionStatus: 'pending',
      fee: donationAmount,
      sendStatus: 'pending',
      provider:provider
    }
    let revenue = await streamerRevenueHelper.create(revenueBody)
    let result = await paymentHelper.makePayment(provider, payment.value, revenue.name, donationAmount, payment.id, revenue.id)
    console.log("createPayment -> result", result,provider,payment.value)
    if (result.code) return res.status(result.code).json(result.res)
    if (provider == 'idpay')
      streamerRevenueHelper.update({authority : result.id},{id:revenue.id})
    let url = result.link || result.url
    return res.status(200).json({ success: true,gateway:url})
  } catch (e) {
    if(e.response && e.response.data)
      e.message = e.response.data.error_message || 'مشکلی پیش آمده است'
    next(e)
  }
}

async function paymentCallback(req, res){
  let {Status,Authority,revenueId,settingId} =req.query
  console.log("paymentCallback -> Status,Authority,revenueId,settingId", Status,Authority,revenueId,settingId)
  if(!settingId || !revenueId)
    return res.status(400).json({message_id:2,success:false,message:'آدرس معتبر نمیباشد'})
  try {
    let revenue = await streamerRevenueHelper.findOneWhere({ id: revenueId })
  if(!revenue)
      return res.status(400).json({ message_id: 3, success: false, message: 'چنین پرداختی در سیستم ثبت نشده است  ' })
   if(revenue.sendStatus=='sent')  return res.status(400).json({ message_id: 3, success: false, message: 'قبلا ثبت شده است ' })
    let paymentGateway = await settingHelper.findOneWhere({ id: settingId })
    let sandbox = false
    if(paymentGateway.value == 'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX' ) sandbox = true
  let zarinpal = zarinPal.zarinPalInstance(paymentGateway.value,sandbox)
  let response = await zarinpal.PaymentVerification({
    Amount: revenue.fee, // In Tomans
    Authority:Authority ,
  })
  if(response.status == 101) return res.status(400).json({success:false,message:zarin.zarinStatus(response.status)})
  console.log(response,'verify res')
    if (response.status === -21) {
      console.log('Empty!');
    } else {
      revenue.refId = response.RefID
    }
    if(Status == 'OK'){
      revenue.transactionStatus = 'purchased'
      alert.sendAlert(paymentGateway.userId,revenueId)

    }else if(Status == 'NOK'){
      revenue.transactionStatus = 'NOK'
    }
    revenue.authority = Authority
    // console.log(revenue,'revenue')
    await revenue.save()
    let responseParams = `status=${Status}`
    goalHelper.send(revenue.userId,revenue.fee)
    return res.redirect(`${config.client}/recipt/${revenue.id}?${responseParams}`) //TODO NANOID
  }catch(e){
    console.log(e)
    let responseParams = `status=NOK`
    return res.redirect(`${config.client}/recipt/${revenue.id}?${responseParams}`) //TODO NANOID
  }
}


async function idPayCallback(req,res,next){
  let {revenueId,settingId} = req.query
  console.log("idPayCallback -> revenueId,settingId", revenueId,settingId)
  try {
    let revenue = await streamerRevenueHelper.findOneWhere({ id: revenueId })
    if(!revenue)
        return res.status(400).json({ message_id: 3, success: false, message: 'چنین پرداختی در سیستم ثبت نشده است  ' })
    if (revenue.sendStatus == 'sent') return res.status(400).json({ message_id: 3, success: false, message: 'قبلا ثبت شده است ' })
    let paymentGateway = await settingHelper.findOneWhere({ id: settingId })
    let result = await idpay.verify(revenue.authority, revenue.id, paymentGateway.value)
    if (result.status != 100) {
      revenue.refId = result.track_id
      revenue.transactionStatus = 'NOK'
      revenue.save()
      let responseParams = `status=NOK`
      return res.redirect(`${config.client}/recipt/${revenue.id}?${responseParams}`) //TODO NANOID
    }
    revenue.refId = result.track_id
    revenue.transactionStatus = 'purchased'
    revenue.save()
    alert.sendAlert(paymentGateway.userId, revenueId)
    goalHelper.send(revenue.userId,revenue.fee)
    let responseParams = `status=OK`
    return res.redirect(`${config.client}/recipt/${revenue.id}?${responseParams}`) //TODO NANOID
  } catch (e) {
    let revenue = await streamerRevenueHelper.findOneWhere({ id: revenueId })
    let responseParams = `status=NOK`
    return res.redirect(`${config.client}/recipt/${revenue.id}?${responseParams}`) //TODO NANOID
  }
}

async function getTitledDonates(req, res, next) {
  let { nanoid, type="latest" } = req.query
  let user = await userHelper.findUserWhere({ nanoid: nanoid })
  if (!user) return false
  let where = `userId = ${user.id} AND transactionStatus='purchased' `
  let now = moment().format("YYYY-MM-DD")
  let aggregate
  let order = 'ORDER BY id DESC'
  switch (type) {
    case 'most':
    aggregate = 'MAX(fee) as value, name'
    break
    case 'latest':
      aggregate = "fee as value, name"
      order = 'ORDER BY createdAt DESC'
      break
    case 'week':
      aggregate = 'MAX(fee) as value, name'
      where += `AND createdAt >= "${moment().add(-7, 'days').format("YYYY-MM-DD")}" AND createdAt <= "${now}"`
    break
    case 'month':
    aggregate = 'MAX(fee) as value, name'
    where += `AND createdAt >= "${moment().add(-30, 'days').format("YYYY-MM-DD")}" AND createdAt <= "${now}"`
    break
    case 'day':
      aggregate = 'MAX(fee) as value, name'
      where += `AND createdAt >= "${moment().format("YYYY-MM-DD")}"`
      break
    default:
      type = "latest"
      aggregate = "fee as value, name"
      order = 'ORDER BY createdAt DESC'
      break
  }
  let donate = await db.sequelize.query(`SELECT ${aggregate} FROM streamerRevenues WHERE ${where} ${order} LIMIT 1`, {
    type: db.sequelize.QueryTypes.SELECT
  })
  return res.send(donate)
}


module.exports = donateRouter







