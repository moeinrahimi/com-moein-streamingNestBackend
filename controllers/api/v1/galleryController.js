const express = require('express');
const galleryHelper = require('_/helpers/db/galleryHelper');
const helper = require('_/helpers');
let galleryController = express.Router();
galleryController.get(`/`, helper.checkToken, galleries);

async function galleries(req, res, next) {
  let userId = req.userId;
  try {
    let userGalleries = await galleryHelper.findAll({ userId: userId });
    let galleries = await galleryHelper.findAll({ status: 'public' });
    let remain = await galleryHelper.sumRemainingSpace({ userId: userId });
    let userVoices = userGalleries.filter((item) => item.type == 'sound');
    let userImages = userGalleries.filter((item) => item.type == 'image');
    let voices = galleries.filter((item) => item.type == 'sound');
    let images = galleries.filter((item) => item.type == 'image');
    return res
      .status(200)
      .json({ success: true, message_id: 0, remain, userVoices, userImages, voices, images });
  } catch (error) {
    console.log(error);
    next(error);
  }
}
module.exports = galleryController;
