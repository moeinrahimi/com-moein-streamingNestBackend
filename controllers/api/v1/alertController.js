const express = require('express');
const alertHelper = require('_/helpers/db/alertHelper');
const userHelper = require('_/helpers/db/userHelper');
const galleryHelper = require('_/helpers/db/galleryHelper');
const helper = require('_/helpers');
const { uploadMedia } = require('_/helpers/upload');
const { io } = require('_/helpers/socketio');
let alertController = express.Router();
alertController.get(`/donations`, helper.checkToken, donationAlert);
alertController.patch(`/:id/donations`, helper.checkToken, updateDonationAlert);
alertController.put(`/media`, helper.checkToken, upload);
alertController.put(`/sound`, helper.checkToken, uploadSound);
alertController.get(`/test`, helper.checkToken, testAlert);
alertController.get(`/public/:nanoid`, publicDonationAlert);
async function donationAlert(req, res, next) {
  let userId = req.userId;
  try {
    let donationSettings = await alertHelper.findOneWhere({ userId: userId });
    return res.status(200).json({ success: true, message_id: 0, donationSettings });
  } catch (error) {
    console.log(error);
    next(error);
  }
}
async function publicDonationAlert(req, res, next) {
  let { nanoid } = req.params;
  try {
    let user = await userHelper.findUserWhere({ nanoid: nanoid });
    let donationSettings = await alertHelper.findOneWhere({ userId: user.id });
    return res.status(200).json({ success: true, message_id: 0, donationSettings });
  } catch (error) {
    console.log(error);
    next(error);
  }
}
async function updateDonationAlert(req, res, next) {
  let alertId = req.params.id;
  let {
    sound,
    media,
    messageTemplate,
    alertAnimationStart,
    alertAnimationEnd,
    alertDuration,
    soundVolume,
    backgroundColor,
    alertDelay,
    layout,
    headerFontSize,
    headerTextAnimation,
    headerColor,
    headerFontFamily,
    headerWordSpacing,
    headerFontWeight,
    donorFontSize,
    donorTextAnimation,
    donorColor,
    donorFontFamily,
    donorWordSpacing,
    donorFontWeight,
  } = req.body;
  console.log(messageTemplate);
  try {
    let donationSettings = await alertHelper.update(
      {
        sound,
        media,
        messageTemplate,
        alertAnimationStart,
        alertAnimationEnd,
        alertDuration,
        soundVolume,
        backgroundColor,
        alertDelay,
        layout,
        headerFontSize,
        headerTextAnimation,
        headerColor,
        headerFontFamily,
        headerWordSpacing,
        headerFontWeight,
        donorFontSize,
        donorTextAnimation,
        donorColor,
        donorFontFamily,
        donorWordSpacing,
        donorFontWeight,
      },
      { id: alertId }
    );
    return res
      .status(200)
      .json({ success: true, message_id: 0, donationSettings, message: 'تنظیمات جدید ذخیره شد' });
  } catch (error) {
    next(error);
  }
}

async function testAlert(req, res, next) {
  let { nanoid, userId } = req;
  try {
    let alert = await alertHelper.findOneWhere({ userId: userId });
    let room = 'ALERT' + nanoid;
    let title = alert.messageTemplate.replace('{نام}', 'علی').replace('{مبلغ}', '50 هزار تومان');
    // let title = alert.messageTemplate.replace("{name}","علی").replace("{amount}","100$")
    alert.title = title;
    alert.donorMessage = 'این یک متن آزمایشی است';
    alert.revenue = { name: 'معین', fee: '10000' };
    io.to(room).emit('DONATE', alert);
    return res.status(200).json({ success: true, message_id: 0, message: 'ارسال شد' });
  } catch (error) {
    next(error);
  }
}

async function upload(req, res, next) {
  req.multerFormats = ['image/gif', 'video/webm', 'video/mp4'];
  try {
    let uploadInstance = uploadMedia('public/uploads/media', 'media', req);
    uploadInstance(req, res, function (err) {
      if (err) return res.status(200).json({ success: true, message_id: 2, message: err });
      return res.status(200).json({ success: true, message_id: 0, name: req.filePath + '/' + req.file.filename });
    });
  } catch (error) {
    next(error);
  }
}
async function uploadSound(req, res, next) {
  req.multerFormats = [
    'audio/webm',
    'audio/mp3',
    'audio/mpeg',
    'image/gif',
    'video/webm',
    'video/mp4',
  ];
  try {
    let uploadInstance = uploadMedia('public/uploads/sounds', 'sound', req);
    uploadInstance(req, res, function (err) {
      if (err) return res.status(200).json({ success: true, message_id: 2, message: err });
      console.log(req.file, 'file type is : ', req.fileType);
      galleryHelper.create({
        file: req.file.filename,
        path: req.filePath + '/' + req.file.filename,
        userId: req.userId,
        status: 'private',
        type: req.fileType,
        size: req.file.size,
      });
      return res.status(200).json({
        success: true,
        message_id: 0,
        name: req.file.filename,
        type: req.fileType,
        message: 'آپلود شد',
      });
    });
  } catch (error) {
    next(error);
  }
}

module.exports = alertController;
