const express = require('express');
const goalHelper = require('_/helpers/db/goalHelper');
const helper = require('_/helpers');
let goalController = express.Router();
goalController.get(`/`, helper.checkToken, getGoal);
goalController.patch(`/:id`, helper.checkToken, updateGoal);
goalController.post(`/`, helper.checkToken, createGoal);

async function getGoal(req, res, next) {
  let userId = req.userId;
  try {
    let goal = await goalHelper.findOneWhere({ userId: userId });
    return res.status(200).json({ success: true, message_id: 0, goal });
  } catch (error) {
    console.log(error);
    next(error);
  }
}

async function updateGoal(req, res, next) {
  let id = req.params.id;
  let {
    title,initial,amount,fontColor,progressColor
  } = req.body;
  try {
    await goalHelper.update(
      {
        title,initial,amount,fontColor,progressColor
      },
      { id: id }
    );
    return res
      .status(200)
      .json({ success: true, message_id: 0, message: 'هدف ویرایش شد' });
  } catch (error) {
    next(error);
  }
}
async function createGoal(req, res, next) {
  let {userId} = req
  let {
    title,initial,amount,fontColor,progressColor
  } = req.body;
  try {
    await goalHelper.create(
      {
        title, initial, amount, fontColor, progressColor,
        userId:userId
      }
    );
    return res
      .status(200)
      .json({ success: true, message_id: 0, message: 'هدف جدید ساخته شد' });
  } catch (error) {
    next(error);
  }
}
module.exports = goalController;
