const express=require('express')
const settingHelper = require('_/helpers/db/settingHelper')
const userHelper = require('_/helpers/db/userHelper')
const helper = require('_/helpers')

let settingRouter = express.Router();


settingRouter.get('/', helper.checkToken,
  getSettings
)

settingRouter.post('/', helper.checkToken,
  postSettings
)

settingRouter.get('/:userName',getPublicSettings)


async function getSettings(req,res,next){
  let { userId } = req
  try {
        console.log(userId ,' user id ')
        let settings = await settingHelper.findAllWhere({ userId })
        return res.status(200).json({success:true,message_id:0,settings})
      }catch(e){
        next(e)
      }
}
async function getPublicSettings(req,res,next){
  let { userName } = req.params
  try {
    let include=['id','userName','avatar','twitch','aparat','telegram','discord','instagram','youtube','twitter','website']
    let user = await userHelper.findUserWhere({ userName },include)
    if(!user) return res.status(400).json({success:false,message:'چنین کاربری وجود ندارد',message_id:2})
    let settings = await settingHelper.findAllWhere({ userId: user.id })
    let index = settings.findIndex((setting,i) => setting.key == 'BANK_zarinpal')
    settings.splice(index,0)
    return res.status(200).json({success:true,message_id:0,settings,user})
  } catch (error) {
    console.log(error)
    next(error)
  }

}
  async function postSettings(req,res,next){
    const { userId } = req
    let data = req.body
    try {
      await settingHelper.createSettings(data,userId)
      return res.status(200).json({success:true,messsage_id:0,message : 'تنظیمات با موفقیت ثبت گردید'})
    }catch(e){
      next(e)
    }
  }

module.exports = settingRouter