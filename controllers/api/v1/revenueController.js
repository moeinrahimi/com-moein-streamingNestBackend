const express=require('express')
const revenueHelper = require('_/helpers/db/streamerRevenueHelper')
const userHelper = require('_/helpers/db/userHelper')
const helper = require('_/helpers')
const db = require('_/models');
let revenueRouter = express.Router();


revenueRouter.get('/', helper.checkToken,getRevenue)
revenueRouter.get('/allTimeFee', helper.checkToken,allTimeRevenue)
revenueRouter.get('/analytics', helper.checkToken,revenueAnalytics)
revenueRouter.get('/:username/leaderboard', getLeaderboard)
revenueRouter.get('/:id/recipt',  getRecipt)

async function getRevenue(req,res,next){
  let { userId } = req
  let where = { userId }
  try {
    let revenues = await revenueHelper.findAllWhere(where)
    return res.status(200).json({success:true,message_id:0,revenues})
  }catch(error){
    next(error)
  }
}

async function allTimeRevenue(req, res, next) {
  let { userId } = req
  let { startDate } = req.query
  let where = { userId }
  try {
    if (startDate) {
      let from = new Date(parseInt(startDate)).toISOString()
      where.createdAt = { $gte: from }
    }
    let allTimeFee = await revenueHelper.countRevenues(where)
    return res.status(200).json({success:true,message_id:0,allTimeFee})
  } catch (error) {
      next(error)
      }
}

async function revenueAnalytics(req, res, next) {
  let { userId } = req
  let { from } = req.query
  let interval = req.query.interval || 'daily'
  try {
    let intervalStmt
    switch(interval){
      case 'days':
      intervalStmt = `DAYOFYEAR(createdAt) as position`
      break
      case 'weeks':
      intervalStmt = `CONCAT(WEEK(date_format(createdAt,'%Y-%m-%d')),'-',date_format(createdAt,'%Y')) as position`
      break
      case 'months':
      intervalStmt = `CONCAT(MONTH(date_format(createdAt,'%Y-%m-%d')),'-',date_format(createdAt,'%Y')) as position`
      break
      case 'years':
      intervalStmt = `date_format(createdAt,'%Y') as position`
      break
    }
    let revenues = await db.sequelize.query(`select date_format(createdAt,'%Y-%m-%d') as date,sum(fee) as total,
    ${intervalStmt}
      from streamerRevenues where userId = :userId AND createdAt >= :from group by position `, {
      replacements: {
        userId,
        from
      },
      type: db.Sequelize.QueryTypes.SELECT
      })

      console.log(revenues.length,'length')
      return res.status(200).json({success:true,message_id:0,revenues})
      }catch(error){
        next(error)
      }
}

async function getLeaderboard(req,res,next){
  let { username } = req.params
  try {
    let user = await userHelper.findUserWhere({username:username})
    let revenues = await revenueHelper.findAllWhere({
      userId: user.id,
      transactionStatus:'purchased'
      },[['fee','DESC']],10,['name','fee','message'])
      return res.status(200).json({success:true,message_id:0,leaderboard:revenues})
      }catch(error){
        console.log("TCL: getLeaderboard -> error", error)
        next(error)
      }
}


async function getRecipt(req,res,next){
  let { id } = req.params
  try {
    let revenue = await revenueHelper.findOneWhere({
      id: id,
      })
    let user = await userHelper.findUserWhere({ id: revenue.userId })
    let recipt = {fee:revenue.fee,refId:revenue.refId,status:revenue.transactionStatus}
      return res.status(200).json({success:true,message_id:0,recipt,user:user.userName})
      }catch(error){
        console.log("TCL: getLeaderboard -> error", error)
        next(error)
      }
}


module.exports = revenueRouter

