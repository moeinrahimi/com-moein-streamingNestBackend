const express = require('express')
const nanoid = require('nanoid')
const jwt = require('jsonwebtoken')
const userHelper = require('_/helpers/db/userHelper')
const alertHelper = require('_/helpers/db/alertHelper')
const settingHelper = require('_/helpers/db/settingHelper')
const helper = require('_/helpers')
const bcrypt = require('bcrypt');
const saltRounds = 10;
let config = require('_/config')
let validator = require('_/helpers/validator')
const fs = require('fs')
const google = require('_/helpers/google')
const twitch = require('_/helpers/twitch')
const queryString = require('querystring');
let regex = new RegExp('^(?=.{2,25}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$');
let userController = express.Router();
userController.post(`/`, createUser)
userController.get(`/`, helper.checkToken,getUser)
userController.get(`/auth`,loginUser)
userController.put(`/`,helper.checkToken,putUser)
userController.get(`/intro`,streamIntro)
userController.get(`/availability`, checkAvailability)

userController.get(`/oauth/google`,redirectToGoogleAuth)
userController.get(`/oauth/google/callback`, googleOauthCallback)

userController.get(`/oauth/twitch`,redirectToTwitchAuth)
userController.get(`/oauth/twitch/callback`,twitchOauthCallback)

async function createUser(req, res,next) {
  let { userName, email, password } = req.body
  if (!userName || !email || !password) return res.status(400).json({ success: false, message: 'فیلد های الزامی تکمیل نشده است', message_id: 2 })
  if(userName.length < 3) return res.status(400).json({ success: false, message: 'نام کاربری نمیتواند کمتر از 3 حرف باشد', message_id:5})
  if (password.length < 5) return res.status(400).json({ success: false, message: 'رمز عبور نمیتواند کمتر از 5 حرف باشد', message_id: 5 })
  let isValidEmail = validator.validate(email)
  if (!isValidEmail) return res.status(400).json({ success: false, message: 'ایمیل وارد شده نامعتبر است', message_id: 5 })
  userName = userName.trim()
  try {
    let exists = await userHelper.findUserWhere({ userName: userName })
    if(exists) return res.status(400).json({ success: false, message: 'نام کاربری انتخابی قبلا ثبت شده است', message_id:4})
    let passwordHash = await bcrypt.hash(password, saltRounds)
    let body = { userName, email, password:passwordHash, nanoid: nanoid(20),avatar:'avatar.png' }
    let user = await userHelper.createUser(body)
    let token = await jwt.sign({ userId: user.id, nanoid: user.nanoid }, config.jwtSecret,{expiresIn:"60d"})
    await alertHelper.create({ userId: user.id, media: '/uploads/media/public/zombie.gif',sound:'/uploads/sounds/public/coin.mp3', messageTemplate: "{نام} {مبلغ} دونیت کرد",alertAnimationStart : 'wobble',alertAnimationEnd:'swing',soundVolume:100,alertDuration:10 })

    settingHelper.createDefaultSettings(user.id)
    return res.status(200).json({ success: true, user,token,message:'حساب کاربری شما با موفقیت ساخته شد' })
  } catch (e) {
    console.log(e)
      next(e)
  }
}

async function getUser(req, res, next) {
  let userId = req.userId
  console.log(userId,'user')
  try {
    let attrs = { exclude: ['password'] }
    let user = await userHelper.findUserWhere({id:userId},attrs)
    return res.status(200).json({success: true,message_id: 0,user})
  }catch(error){
      // console.log(error)
      next(error)
  }
}
async function loginUser(req, res, next) {
  try {

  let credentials = req.headers.authorization.split(" ")[1]
  credentials = new Buffer(credentials, "base64").toString()
  credentials = credentials.split(":")
  credentials = {
    username: credentials[0],
    password: credentials[1]
  }
    let user = await userHelper.findUserWhere({ userName: credentials.username })
    if (!user) return res.status(400).json({ success: false, message_id: 2, message: 'نام کاربری یا رمز عبور اشتباه است' })
    // let passwordHash = await bcrypt.compare(credentials.password, user.password);
    // if (!passwordHash) {
    //   console.log( 'wrong pass')
    //   return res.status(400).json({success: false,message_id: 2,message: 'نام کاربری یا رمز عبور اشتباه است'})
    // }
    let token = await jwt.sign({ userId: user.id, nanoid: user.nanoid }, config.jwtSecret, { expiresIn: "60d" })
    delete user.password
    return res.status(200).json({success: true,user,token,message_id: 0,message: ''})
  } catch (error) {
    // console.log(error)
      next(error)
    }
}

async function putUser(req, res, next) {
  let { userId } = req
  let {avatar,userName,email,password,newPassword,telegram,instagram,twitter,youtube,website,discord,aparat,twitch}=req.body
  try {
    let updateFields = { avatar, userName, telegram, instagram, twitter, youtube, website, discord, aparat, twitch }
    if (userName) {
      let isValid = regex.exec(userName)
      if(!isValid)  return res.status(400).send({message:"نام کاربری میتواند شامل حروف الفبای انگلیسی و اعداد باشد"})
    }
    if (password && newPassword) {
      let dbUser = await userHelper.findUserWhere({ id: userId })
      if (!dbUser) return status(400).json({ success: false, message: 'کاربر یافت نشد' })
      let passwordHash = await bcrypt.compare(password, dbUser.password);
      if (!passwordHash) {
        return res.status(400).json({success: false,message_id: 2,message: 'رمز عبور اشتباه است'})
      }
      let newPasswordHash = await bcrypt.hash(newPassword,saltRounds)
      updateFields.password = newPasswordHash
    }
    console.log(updateFields)
    await userHelper.update(updateFields,{id:userId})
    return res.status(200).json({success: true,message_id: 0,message: 'پروفایل شما با موفقیت بروز شد'})
    }catch(error){
      next(error)
    }
}
function streamIntro(req, res) {
  let path = './public/intro.mp4'
  const stat = fs.statSync(path)
  const fileSize = stat.size
  const range = req.headers.range
  if (range) {
    const parts = range.replace(/bytes=/, "").split("-")
    const start = parseInt(parts[0], 10)
    const end = parts[1]
      ? parseInt(parts[1], 10)
      : fileSize-1
    const chunksize = (end-start)+1
    const file = fs.createReadStream(path, {start, end})
    const head = {
      'Content-Range': `bytes ${start}-${end}/${fileSize}`,
      'Accept-Ranges': 'bytes',
      'Content-Length': chunksize,
      'Content-Type': 'video/mp4',
      'Max-Age':'604800000' //7d
    }
    res.writeHead(206, head);
    file.pipe(res);
  } else {
    const head = {
      'Content-Length': fileSize,
      'Content-Type': 'video/mp4',
    }
    res.writeHead(200, head)
    fs.createReadStream(path).pipe(res)
  }
}

async function checkAvailability(req, res, next) {
  const {username} = req.query
  try {

    let regex = new RegExp('^(?=.{2,25}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$');
    let isValid = regex.exec(username)
    if(!isValid)  return res.status(400).send({message:"نام کاربری میتواند شامل حروف الفبای انگلیسی و اعداد باشد"})
    let user = await userHelper.findUserWhere({ userName: username })
    console.log("checkAvailability -> user", user)
    if (user) {
      return res.status(400).send({message:'این نام قبلا ثبت شده است'})
    }
    return res.status(200).send({message:"این نام کاربری قابل ثبت میباشد"})
  }catch(e){
    next(e)
  }
}

async function redirectToGoogleAuth(req,res,next){
 try {
   let url = google.googleLoginUrl
   return res.redirect(url)
 }catch(e){
   console.log(e,'redirectToGoogleAuth')
   next(e)
 }
}

async function googleOauthCallback(req, res, next) {
  let {code}=req.query
 try {
    let access_token= await google.getAccessTokenFromCode(code)
    let userData = await google.getUserInfo(access_token)
    let user = await userHelper.findUserWhere({ email: userData.email })
    let newUser= false
    if (!user) {
      newUser= true
    user = await userHelper.createUser({
      email: userData.email,
      avatar: userData.picture,
      fname: userData.name,
      nanoid:nanoid(20)
    })
    await alertHelper.create({ userId: user.id, media: '/uploads/media/public/zombie.gif',sound:'/uploads/sounds/public/coin.mp3', messageTemplate: "{نام} {مبلغ} دونیت کرد",alertAnimationStart : 'wobble',alertAnimationEnd:'swing',soundVolume:100,alertDuration:10 })
    settingHelper.createDefaultSettings(user.id)
   }
   if(!user.userName) newUser = true
    let token = await jwt.sign({ userId: user.id, nanoid: user.nanoid }, config.jwtSecret, { expiresIn: "60d" })
    delete user.password
   let params = queryString.stringify({ success: true, user, token, message_id: 0, oauth: 'google', newUser: newUser })
    return res.redirect(`${config.client}/dashboard?${params}`)

 }catch(e){
   console.log(e,'redirectToGoogleAuth')
   next(e)
 }
}


async function redirectToTwitchAuth(req,res,next){
  try {
    return res.redirect(twitch.loginUrl)
  }catch(e){
    console.log(e,'redirectToTwitchAuth')
    next(e)
  }
 }

async function twitchOauthCallback(req, res, next) {
  let {code}=req.query
 try {
   let tokenResult = await twitch.getToken(code)
   console.log("twitchOauthCallback -> code", code)
   let {access_token} = tokenResult.data
   let userData = await twitch.getUser(access_token).catch(e=>console.log(e,'getuser'))
   userData = userData.data.data[0]
   console.log("twitchOauthCallback -> userData", userData)
   let user = await userHelper.findUserWhere({ twitchId: userData.id }) // twitch id is unique -- username can change over time
   if(!user) user = await userHelper.findUserWhere({ userName: userData.login }) // for users logined before without twitch
    let newUser = false
    if (!user) {
      user = await userHelper.createUser({
        userName:userData.login,
        email: userData.email,
        avatar: userData.profile_image_url,
        fname: userData.display_name,
        twitchId: userData.id,
        nanoid:nanoid(20)
      })

    await alertHelper.create({ userId: user.id, media: '/uploads/media/public/zombie.gif',sound:'/uploads/sounds/public/coin.mp3', messageTemplate: "{نام} {مبلغ} دونیت کرد",alertAnimationStart : 'wobble',alertAnimationEnd:'swing',soundVolume:100,alertDuration:10 })
    settingHelper.createDefaultSettings(user.id)
    } else {
      // user.userName = userData.login
      user.email = userData.email
      user.fname = userData.display_name
      user.avatar = userData.profile_image_url
      user.save()
   }
    let token = await jwt.sign({ userId: user.id, nanoid: user.nanoid }, config.jwtSecret, { expiresIn: "60d" })
    delete user.password
    let params = queryString.stringify({ success: true, user, token, message_id: 0, oauth: 'twitch', newUser: newUser })
    return res.redirect(`${config.client}/dashboard?${params}`)

 }catch(e){
   console.log(e.response.data,'twitchOauthCallback')
   next(e)
 }
}


module.exports = userController
