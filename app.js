require('module-alias/register');
let express = require('express');
let app = express();
let http = require('http').Server(app);
const { errorHandlerMiddleware } = require('_/helpers/errorHandler');
const helmet = require('helmet');
const io = require('socket.io')(http, {
  path: '/app/socket/',
  // transports: ['websocket', 'xhr-polling']
});
module.exports.io = io;
let path = require('path');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let config = require('./config');
let userController = require('./controllers/api/v1/userController');
let alertController = require('./controllers/api/v1/alertController');
let settingController = require('./controllers/api/v1/settingController');
let donateController = require('./controllers/api/v1/donateController');
let revenueController = require('./controllers/api/v1/revenueController');
let goalController = require('./controllers/api/v1/goalController');
let galleryController = require('./controllers/api/v1/galleryController');

let db = require('./models');

db.sequelize
  .sync
  // {force: true}
  ()
  .catch((err) => {
    console.log(`Sequelize issue:\nerr name :${err.name}\nerr message :  ${err.message}`);
  });

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/app', express.static(path.join(__dirname, 'public')));

//only for developments
let cors = require('cors');
app.use(cors());
app.use(helmet());

app.use('/app/api/v1/users', userController);
app.use('/app/api/v1/alerts', alertController);
app.use('/app/api/v1/settings', settingController);
app.use('/app/api/v1/donates', donateController);
app.use('/app/api/v1/revenues', revenueController);
app.use('/app/api/v1/goals', goalController);
app.use('/app/api/v1/galleries', galleryController);
// catch 404
app.use(function (req, res, next) {
  return res.status(404).send('NOT FOUND');
});

app.use(errorHandlerMiddleware);

http.listen(config.port, '0.0.0.0', (e) => {
  console.log('listening on port : ' + config.port);
});

module.exports = app;
