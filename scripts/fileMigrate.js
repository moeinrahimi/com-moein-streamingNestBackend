const db = require('_/models')
const fs = require('fs');

let run = async () => {
  let soundPath = 'public/uploads/sounds';
  let mediaPath = 'public/uploads/media';
  let alerts = await db.AlertBox.findAll({
    // where: { userId: 80 },
    group: ['userId'],
    attributes: ['sound', 'media', 'userId','id'],
    // raw: true,
  });
  let users = await db.User.findAll({
    // raw: true,
  });
  let settings = await db.Setting.findAll({
    where: {
      key: ["DONATE_bannerImage","DONATE_backgroundImage"]
    },
    // raw: true,
  });

  for (let i = 0; i < settings.length; i++) {
    const setting = settings[i];
    let media = mediaPath + '/' + setting.value;
    let newMediaPath = 'public/uploads/media/' + setting.userId;
    if (!fs.existsSync(newMediaPath)) {
      fs.mkdirSync(newMediaPath);
    }
    fs.copyFile(media, newMediaPath + '/' + setting.value, (err) => {
      if (err) {
        if(setting.key == 'DONATE_bannerImage') newMediaPath = '/uploads/media/sampleBanner.png'
        if (setting.key == 'DONATE_backgroundImage') newMediaPath = '/uploads/media/sampleImage.png'
        setting.value = newMediaPath
        setting.save()

      } else {
        setting.value = '/uploads/media/' + setting.userId + '/' + setting.value
        setting.save()
      }

    });
  }

  for (let i = 0; i < users.length; i++) {
    const user = users[i];
    if (!user.avatar || user.avatar.startsWith('http')) continue;
    let media = mediaPath + '/' + user.avatar;
    let newMediaPath = 'public/uploads/media/' + user.id;
    if (!fs.existsSync(newMediaPath)) {
      fs.mkdirSync(newMediaPath);
    }
    fs.copyFile(media, newMediaPath + '/' + user.avatar, (err) => {
      if (!err) {
        user.avatar = '/uploads/media/' + user.avatar
        user.save()
      }
  //     // console.log('media was copied to destination.txt');
    });
  }

  for (let i = 0; i < alerts.length; i++) {
    const alert = alerts[i];

    if (!alert.sound && !alert.media) continue
    if(!alert.userId) continue
    let sound = soundPath + '/' + alert.sound
    let media = mediaPath + '/' + alert.media
    let newMediaPath = 'public/uploads/media/' + alert.userId
    if (!fs.existsSync(newMediaPath)){
      fs.mkdirSync(newMediaPath);
    }
    let newSoundPath = 'public/uploads/sounds/' + alert.userId
    if (!fs.existsSync(newSoundPath)){
      fs.mkdirSync(newSoundPath);
    }
    if (alert.sound) {
      console.log(alert.sound)
      fs.copyFile(sound, newSoundPath+'/'+alert.sound, (err) => {
        if (err) console.log( err);
        // console.log('sound was copied to destination.txt');
        alert.sound = '/uploads/sounds/' + alert.userId + '/' + alert.sound
        alert.save()
      });
    }
    if (alert.media) {
      console.log(alert.media)
    fs.copyFile(media, newMediaPath+'/'+alert.media, (err) => {
      if (err) console.log( err);
      // console.log('media was copied to destination.txt');
      alert.media = '/uploads/media/' + alert.userId + '/' + alert.media
      alert.save()
    });
    }

  }
};
run();