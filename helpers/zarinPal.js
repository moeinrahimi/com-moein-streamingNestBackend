const config = require('_/config');

const zarinPalInstance = (token = 'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX',sandbox=false) => {
  const zarinpal = require('zarinpal-checkout').create(token,sandbox)
  return zarinpal
}
const requestPayment = async (apiKey, name, amount, settingId, revenueId) => {
  let callback = `${config.baseUrl}/api/v1/donates/callback?settingId=${settingId}&revenueId=${revenueId}`;
  let zarinpalInstance = await zarinPalInstance(apiKey);
  let data = await zarinpalInstance.PaymentRequest({
    Amount: amount, // In Tomans
    CallbackURL: callback,
    Description: 'حمایت مالی',
  });
  console.log(amount,callback,apiKey)
  let zarinPalMessage = codeToMessage(data.status);
  if (data.status != 100) return { code: 400, res: { success: false, message: zarinPalMessage } }
  return data
};

const codeToMessage = (code) => {
const data = {
'-1' : 'اطلاعات ارسال شده ناقص است.',
'-2' : 'IP و يا مرچنت كد پذيرنده صحيح نيست.',
'-3' : 'با توجه به محدوديت هاي شاپرك امكان پرداخت با رقم درخواست شده ميسر نمي باشد.',
'-4' : 'سطح تاييد پذيرنده پايين تر از سطح نقره اي است.',
'-11' : ' درخواست مورد نظر يافت نشد.',
'-12' : ' امكان ويرايش درخواست ميسر نمي باشد.',
'-21' : ' هيچ نوع عمليات مالي براي اين تراكنش يافت نشد.',
'-22' : ' تراكنش نا موفق ميباشد.',
'-33' : ' رقم تراكنش با رقم پرداخت شده مطابقت ندارد.',
'-34' : ' سقف تقسيم تراكنش از لحاظ تعداد يا رقم عبور نموده است',
'-40' : ' اجازه دسترسي به متد مربوطه وجود ندارد.',
'-41' : ' اطلاعات ارسال شده مربوط به AdditionalData غيرمعتبر ميباشد.',
'-42' : ' مدت زمان معتبر طول عمر شناسه پرداخت بايد بين 30 دقيه تا 45 روز مي باشد.',
'-54' : ' درخواست مورد نظر آرشيو شده است.',
'100' : ' عمليات با موفقيت انجام گرديده است.',
'101' : ' عمليات پرداخت موفق بوده و قبلا PaymentVerification تراكنش انجام شده است.',
}
return data[code]
}

module.exports = {
  zarinPalInstance,
  codeToMessage,
  requestPayment

}

