const db = require('_/models')
const userHelper = require('./userHelper')
const { io } = require('_/app')

async function create(body) {
  return db.Goal.create(body)

}
async function findOneWhere(where,options={}){
  return db.Goal.findOne({where:where,...options})

}
async function update(updatedFields,where){
  return db.Goal.update(updatedFields,{where:where})

}

async function initGoal(roomName,nanoid){
  let user = await userHelper.findUserWhere({ nanoid: nanoid })
  let goal = await findOneWhere({ userId: user.id },{raw:true})
  io.to(roomName).emit("GOAL_PROGRESS",{...goal})
}

async function send(userId, donateAmount) {
  let user = await userHelper.findUserWhere({id:userId})
  let goal = await findOneWhere({ userId: user.id })
  goal.initial = Number(goal.initial) + Number(donateAmount)
  goal.save()
  let roomName = "ALERT" + user.nanoid
  let { amount,initial,title,fontColor,progressColor }= goal
  io.to(roomName).emit("GOAL_PROGRESS",{amount,initial,title,fontColor,progressColor})
}

module.exports = {create,findOneWhere,update,initGoal,send}