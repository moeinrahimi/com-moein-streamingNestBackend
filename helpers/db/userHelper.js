const db = require('../../models')
async function findUserWhere(where, attributes={}) {

  return db.User.findOne(
    {
      where: where,
      attributes:attributes,
    }
  )

}
async function createUser(body) {
  return db.User.create(body)

}
async function update(updatedFields, where) {
  return db.User.update(updatedFields,{where:where})

}

module.exports = {findUserWhere,createUser,update}