const db = require('_/models')
const fs = require('fs')
async function findAllWhere(where) {
  return db.Setting.findAll({
    where: where,
  })
}

async function findOneWhere(where) {
  return db.Setting.findOne({
    where: where,
  })
}

async function findOrCreate(where,fields){
   return db.Setting.findOrCreate({
    where:where,
    defaults:fields
  })
}

async function createSettings(fields, userId) {
  Object.keys(fields).forEach(async key=>{
    let where = {userId,key}
    let field = { value: key == 'DONATE_presetAmountItems' ? JSON.stringify(fields[key]) : fields[key] }
    let [setting, created] = await findOrCreate(where, field)
    if(!created && setting.value!=fields[key]){
      setting.value = field.value
      setting.save()
    }
  })
}

async function createDefaultSettings(userId) {
  let newMediaPath = 'public/uploads/media/' + userId;
  let newSoundPath = 'public/uploads/sounds/' + userId;
  if (!fs.existsSync(newMediaPath)) {
    fs.mkdirSync(newMediaPath);
  }
  if (!fs.existsSync(newSoundPath)) {
    fs.mkdirSync(newSoundPath);
  }

  const fields = {
    DONATE_minimumAmount: 1000,
    DONATE_messageLength: 255,
    DONATE_buttonColor:'#000',
    DONATE_description:'',
    DONATE_showLeaderboard:false,
    DONATE_showLeaderboardAmount:false,
    DONATE_bannerImage:'/uploads/media/sampleBanner.png',
    DONATE_backgroundImage:'/uploads/media/sampleImage.png',
    DONATE_presetAmount:true,
    DONATE_presetAmountItems:[1000,5000,10000],
  }
  return createSettings(fields,userId)
}

async function activeGateway(userId) {
  let defaultPayment = await module.exports.findOneWhere({ key: 'paymentGateway', userId: userId })
  if (!defaultPayment) return { code: 400, res : { success: false, message: 'درگاه فعال انتخاب نشده است', message_id: 4 } }
  let payment = await module.exports.findOneWhere({ key: defaultPayment.value, userId: userId })
  if (!payment || !payment.value) return { code: 400, res: { success: false, message: 'اطلاعات پرداخت وارد نشده است', message_id: 5 } }
  return payment

}

module.exports = {findAllWhere,findOrCreate,createSettings,createDefaultSettings,findOneWhere,activeGateway}