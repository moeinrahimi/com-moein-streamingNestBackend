const db = require('_/models')

async function create(body) {
  console.log(body,'crea')
  return db.Gallery.create(body)

}
async function findAll(where){
  return db.Gallery.findAll({where:where,    order:[['createdAt','DESC']]
})
}

async function sumRemainingSpace(where){
  return db.Gallery.findAll({
    where: where,
    subQuery:false,
    attributes: [
      [db.sequelize.fn('sum', db.sequelize.col('size')), 'size']
    ],
    group: ['userId'],
  })

}

async function update(updatedFields,where){
  return db.Gallery.update(updatedFields,{where:where})

}



module.exports = {create,findAll,update,sumRemainingSpace}