const db = require('../../models');
async function findOneWhere(where) {
  return db.StreamerRevenue.findOne({
    where: where,
    // attributes: include
  });
}

async function findAllWhere(where, order = [['createdAt', 'DESC']], limit = 100, attributes) {
  let da = {
    where: where,
    order: order,
    limit:limit,
  }
  if(attributes) da.attributes=attributes
  return db.StreamerRevenue.findAll(da);
}

async function countRevenues(where) {
  return db.StreamerRevenue.findOne({
    attributes: [
      [db.Sequelize.fn("SUM", db.Sequelize.col("fee")), "fee"],
      [db.Sequelize.fn("COUNT", db.Sequelize.col("fee")), "count"]
    ],
    where: where
  });
}

async function create(body) {
  return db.StreamerRevenue.create(body);
}

async function update(updatedFields,where){
  return db.StreamerRevenue.update(updatedFields,{where:where})

}


module.exports = {
  findOneWhere,
  create,
  findAllWhere,
  countRevenues,
  update
};
