const db = require('../../models');
async function findOneWhere(where, include = ['*']) {
  return db.Donor.findOne({
    where: where,
    attributes: include
  });
}
async function create(body) {
  console.log(body)
  return db.Donor.findOrCreate({
    where: {
      streamerRevenueId:body.streamerRevenueId
    },
    defaults:body
  });
}

module.exports = { findOneWhere,create };
