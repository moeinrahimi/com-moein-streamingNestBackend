const Sentry = require('@sentry/node');
Sentry.init({ dsn: 'https://ac3b10e98db24cd1ac776fbe149d512b@o404093.ingest.sentry.io/5267387' });
// const logger = require('./loki')
// const messages = require("_/components/messages");
const config = require("_/config");
// const isProduction = process.env.production
// const discord = require("../components/discord");
// const TelegramLogger = require("node-telegram-logger");
// let tl = new TelegramLogger(config.telegramToken, config.telegramChannel);


const logIt = (req, error) => {

  try {
    if (isProduction) {
      let msg = messages.tgMessage(error, req);
      tl.sendMessage(msg);
      discord.sendMessage(msg);
    }
  } catch (e) {
    console.log("ERROR HAPPENED", e.code, e.message, "logIt");
  }
};
function errorHandlerMiddleware(err, req, res, next) {
  let message = "مشکلی پیش آمده است";
  console.log("error passed to errorHandlerMiddleware : " + err);
  // logger.error("ERRMIDDLEWARE",err)
  if (process.env.NODE_ENV == 'production') {
    Sentry.configureScope(scope => {
      scope.setUser({
        id: req.userId || 'not loggedIn',
        // username: req.username || 'not loggedIn',
        // ip:req.ip
      })
      scope.setExtra("URL",req.originalUrl)
      scope.setExtras(req.query)
      scope.setExtras(req.body)
      Sentry.captureException(err);
    })
    // app.use(Sentry.Handlers.requestHandler());
  // app.use(Sentry.Handlers.errorHandler());
  }
  return res
    .status(500)
    .json({ success: false, message_id: 1, message: message,reason:err.message });
}
module.exports = {
  // logIt,
  // tl,
  errorHandlerMiddleware
};


process
  .on('unhandledRejection', (reason, p) => {
    Sentry.captureException(reason);

    console.log(reason, 'Unhandled Rejection at Promise', p);
  })
  .on('uncaughtException', err => {
    Sentry.captureException(err);

    console.log(err, 'Uncaught Exception thrown');
  });