const config =require('_/config')
const queryString = require('querystring');
const axios = require('axios');
const clientid = '7rr8zh3zkd5wkgf2lrm1mreywzybgl'
const clientSecret = 'qx85el5vjart4bfpf44fjehpye3b9d'
const stringifiedParams = queryString.stringify({
  client_id: clientid,
  redirect_uri: config.baseUrl + '/api/v1/users/oauth/twitch/callback',
  response_type:'code',
  scope: [
    'user:read:email',
    'openid',
    'user_read',
  ].join(' '), // space seperated string
});

const loginUrl = `https://id.twitch.tv/oauth2/authorize?${stringifiedParams}`;
// console.log("loginUrl", loginUrl)

const getUser = async (access_token) => {
  return axios({
    url: "https://api.twitch.tv/helix/users",
    method: 'get',
    headers: {
      "Authorization": `Bearer ${access_token}`,
      'Client-ID': clientid
    }
  })

}

const getToken = async (code) => {
  return axios({
    url: "https://id.twitch.tv/oauth2/token",
    method: 'post',
    data:queryString.stringify({
      client_id: clientid,
      client_secret: clientSecret,
      code:code,
      grant_type:'authorization_code',
      redirect_uri: config.baseUrl + '/api/v1/users/oauth/twitch/callback',
    })
  })

}
module.exports = {
  loginUrl,
  getUser,
  getToken

}