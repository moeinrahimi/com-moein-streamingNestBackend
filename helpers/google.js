const config = require('_/config')
const queryString = require('querystring');
const axios = require('axios');
const clientid = '1086308967884-5brv0uukvslbsc1fa5s6v2oihqfr8udg.apps.googleusercontent.com'
const clientsecret = 'o3A2Ms3WCFNVZ2-xi3kWi54T'
const stringifiedParams = queryString.stringify({
  client_id: clientid,
  redirect_uri: config.baseUrl + '/api/v1/users/oauth/google/callback',
  scope: [
    'https://www.googleapis.com/auth/userinfo.email',
    'https://www.googleapis.com/auth/userinfo.profile',
  ].join(' '), // space seperated string
  response_type: 'code',
  access_type: 'offline',
  prompt: 'consent',
});

const googleLoginUrl = `https://accounts.google.com/o/oauth2/v2/auth?${stringifiedParams}`;
// console.log(googleLoginUrl,'aaaaaaa')

async function getAccessTokenFromCode(code) {
  const r = await axios({
    url: `https://oauth2.googleapis.com/token`,
    method: 'post',
    data: {
      client_id: clientid,
      client_secret: clientsecret,
      redirect_uri: config.baseUrl + '/api/v1/users/oauth/google/callback',
      grant_type: 'authorization_code',
      code,
    },
  }).catch(e=>console.log(e.response.data));
  console.log(r.data); // { access_token, expires_in, token_type, refresh_token }
  return r.data.access_token;
};

async function getUserInfo(access_token) {
  const { data } = await axios({
    url: 'https://www.googleapis.com/oauth2/v2/userinfo',
    method: 'get',
    headers: {
      Authorization: `Bearer ${access_token}`,
    },
  });
  // console.log(data); // { id, email, given_name, family_name }
  return data;
};
// const urlParams = queryString.parse('code=4%2FywFIFkDPx9c8UjX_6vZ2rPOpFZPfOV_ZeY5J_jvpRcJQrBZ84rC65TEVaT-4NagF9Ird1nJFKcbjxhYuUNsP9HU&scope=email+profile+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.profile+openid&authuser=0&prompt=consent#');

// if (urlParams.error) {
//   console.log(`An error occurred: ${urlParams.error}`);
// } else {
//   console.log(`The code is: ${urlParams.code}`);
//   getAccessTokenFromCode(urlParams.code)
//   // {
//   //   access_token: 'ya29.Il-8B6OQC7i8QspeKPH5LJ75ir_YF_W55wRimaZftpPPDwBleSp_Dbokl7WRv3HUm24NuRy15UAnW35kZMsJHLMoUeuJbkiXYqqe5N9bSLIYa5nK3B3JBwzc5wiN-wDIpg',
//   //   expires_in: 3599, in seconds
//   //   refresh_token: '1//03E-wcYMo8_WjCgYIARAAGAMSNwF-L9IrPY8m9OHSog_ud9CWDXviC7-MoeV_v67_X9ehEoapY7DAMr8TJzZncIgWa4WWp2G4MTE',
//   //   scope: 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile openid',
//   //   token_type: 'Bearer',
//   //   id_token: 'eyJhbGciOiJSUzI1NiIsImtpZCI6ImIyZWQwZGIxZjY2MWQ4OTg5OTY5YmFiNzhkMmZhZTc1NjRmZGMzYTkiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiI1MjAxMzc0MzY4ODItdGFscWNrdmYydjJrMmZ1bHEyMDVoOTU2c2poMGw0NWkuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI1MjAxMzc0MzY4ODItdGFscWNrdmYydjJrMmZ1bHEyMDVoOTU2c2poMGw0NWkuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDcwMTE2MjczMzEyNzQwNDE5NzQiLCJlbWFpbCI6Im0ucmFoaW1pMjE1MEBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXRfaGFzaCI6Imk5VTE5RlZZSW11NzJNbloxZ1VMeWciLCJuYW1lIjoiTW9laW4gUmFoaW1pIiwicGljdHVyZSI6Imh0dHBzOi8vbGg2Lmdvb2dsZXVzZXJjb250ZW50LmNvbS8tcEZHTWpCbF95QWMvQUFBQUFBQUFBQUkvQUFBQUFBQUFBQUEvQUNIaTNyY1d3eFY4WWN0VEhrbGJZUGZ2d1c4SVI0MUNoQS9zOTYtYy9waG90by5qcGciLCJnaXZlbl9uYW1lIjoiTW9laW4iLCJmYW1pbHlfbmFtZSI6IlJhaGltaSIsImxvY2FsZSI6ImVuIiwiaWF0IjoxNTgwNTQ5MzM1LCJleHAiOjE1ODA1NTI5MzV9.oJfvNoadwChd3vZ6jj4qSP6og51dzqMv2c7roJSBUHaneG75rn_Ci10QZSyVoF2fNpWEbFirYjnntlUULbfsab74BvhLyJ9OvI4FpBk7uMBpFDG26W2_q7tkT32kL0MTSfg1XesDSXtacPgCM_jnWwAQl79DsPybDBF7TSqfpPcbzB7FiOzx3cbGZhG8FmBzDfkk35ta5ZxsFtnk_yeXouATpW_qp1zk66yJgIAPsPVTRu-YwVY5-DI_B5hb-V4WUYMsPHTUg5QnTKOs_-rzWdrwsyji3CBIEwxC8cbCsjnt_doeYOUZ8KPHBPXH26tEbKXq0UUKtMt4zR1JDh78Ew'
//   // }

// }

// getUserInfo('ya29.Il-8Bxhv9m2wAsEP2k4zsleL3aWIpsiIe1BhSePxbkpRLf5luvDEau8px51MpFY8mpW_OWaqokMxI1N1_N3f1BSqG6BYgNpzCjW_MQDwuzaA1WxPqL7uZKy8DaDOB7xvWA')

// {
//   id: '107011627331274041974',
//   email: 'm.rahimi2150@gmail.com',
//   verified_email: true,
//   name: 'Moein Rahimi',
//   given_name: 'Moein',
//   family_name: 'Rahimi',
//   picture: 'https://lh6.googleusercontent.com/-pFGMjBl_yAc/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rcWwxV8YctTHklbYPfvwW8IR41ChA/photo.jpg',
//   locale: 'en'
// }

// refresh_token: '1//091v6UQFiVzoyCgYIARAAGAkSNwF-L9IrRJkMecEpaXoyvqu2UUQYoKeQnR-MZOXLKSQX2I6zv7TQBcpafTC045xVpIva5jccxUE',
module.exports = {
  googleLoginUrl,
  getAccessTokenFromCode,
  getUserInfo
}