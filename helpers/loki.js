const { createLogger } = require("winston");
const LokiTransport = require("winston-loki");
const options = {
  transports: [
    new LokiTransport({
      host: "http://127.0.0.1:3100",
      labels:{ module: 'server' }
    })
  ]
};
const logger = createLogger(options);
module.exports = logger