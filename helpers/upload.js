const mime = require('mime')
const multer = require('multer')
function fileFilter(req, file, cb) {
  // console.log(fileFilter,'',file.mimetype)
const baseFormats = ['image/png', 'image/jpeg']
const multerFormats = req.multerFormats ? (Array.isArray(req.multerFormats) ? req.multerFormats:[]):[]
const authorizedFormats = [...baseFormats, ...multerFormats]
  if (authorizedFormats.includes(file.mimetype)) {
      cb(null, true)
  } else {
      console.log('inja')
      cb('فرمت تصویر مجاز نمیباشد',false)
  }
}
function uploadMedia(directory,inputName = 'avatar') {
  if(!directory) throw new Error('no directory in args, uploadImage FUNCTION')
  var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      let ext = mime.getExtension(file.mimetype)
      if (["gif", 'webm', 'mp4', 'jpeg', 'jpg', 'png','3gp'].includes(ext))
      {
        req.fileType = 'image'
        directory = '/uploads/media/' + req.userId
        req.filePath = directory
        return cb(null, 'public'+directory)
      }else if(["mp3", 'webm','mpga','wav','ogg','flac'].includes(ext))
        directory = '/uploads/sounds/'  + req.userId
      req.fileType = 'sound'
      req.filePath = directory
        return  cb(null, 'public'+directory)
      },
      filename: function (req, file, cb) {
        let extenstion = mime.getExtension(file.mimetype)
        let fileName = Math.round(Math.random() * 785542)   + '.' + extenstion
        cb(null,  fileName)
      }
    })
    var upload = multer({ storage: storage,fileFilter:fileFilter }).single(inputName)
    return upload
}
// req.multerFormats = ['application/pdf']
// let upload = imageUtils.uploadImage(basePath,'lab')
// upload(req, res, function (err, data) {
//   if (err) {
//       console.log(err)
//   }
//   if (req.file == undefined) {
//       return res.status(400).json({ success: false, message_id: 10, message: 'تصویری ارسال نشده است' })
//   }
//   let imageName = req.file.filename
//   return res.status(200).json({ success: true, message_id: 0, message: 'تصویر آپلود شد',imageName })
// })
module.exports = {
  uploadMedia
}