const axios = require('axios')
const config = require('_/config')
exports.requestPayment = async (apiKey,name,amount,settingId,revenuId) => {
  let { data }=await axios({
    url: 'https://api.idpay.ir/v1.1/payment',
    method: 'post',
    headers: {
      "X-API-KEY": apiKey,
      // "X-SANDBOX":true
    },
    data: {
      "order_id": revenuId,
      "amount":   amount * 10 ,
      "name":     name,
      // "mail":     email,
      "callback": config.baseUrl + `/api/v1/donates/idpay/callback?settingId=${settingId}&revenueId=${revenuId}`,

    }
  })
  return data

}

exports.verify = async (id,order_id,apiKey) => {
  let { data }=await axios({
    url: 'https://api.idpay.ir/v1.1/payment/verify',
    method: 'post',
    headers: {
      "X-API-KEY": apiKey,
      // "X-SANDBOX":true
    },
    data: {
      "id": id,
      "order_id": order_id,

    }
  })
  return data

}

