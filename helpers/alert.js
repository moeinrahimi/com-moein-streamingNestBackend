const { io } = require('_/helpers/socketio')
const alertHelper = require('_/helpers/db/alertHelper')
const userHelper = require('_/helpers/db/userHelper')
const streamerRevenueHelper = require('_/helpers/db/streamerRevenueHelper')
// var Queue = require('bull');
// var alertQueue = new Queue('donation alert queue', 'redis://127.0.0.1:6379');

// alertQueue.process(function (job, done) {
//   let {alert,user,revenue} = job.data
//   let room = "ALERT" + user.nanoid
//   let title = alert.messageTemplate.replace("{name}",revenue.name).replace("{amount}",revenue.fee)
//   alert.title = title
//   alert.donorMessage = revenue.message
//   io.to(room).emit('DONATE', alert)
//   streamerRevenueHelper.update({
//     sendStatus: 'sent',
//   }, {
//     id:revenue.id
//   })
//   done();

// });
const sendAlert = async (userId,revenueId) => {
  try {
    let [alert,user,revenue] = await Promise.all([
      alertHelper.findOneWhere({ userId: userId }),
      userHelper.findUserWhere({ id: userId }),
      streamerRevenueHelper.findOneWhere({ id: revenueId })
    ])
    // let alert = {}
    let room = "ALERT" + user.nanoid
    let title = alert.messageTemplate.replace("{نام}", revenue.name).replace("{مبلغ}", revenue.fee + 'هزار تومان')
    alert.revenue = {name:revenue.name,fee:revenue.fee}
    alert.title = title
    alert.donorMessage = revenue.message
    io.to(room).emit('DONATE', alert)
    streamerRevenueHelper.update({
      sendStatus: 'sent',
    }, {
      id:revenue.id
      })
    // console.log(revenues.length, 'revenues')
    // let timer = (alert.alertDuration * 1000) + (alert.alertDelay * 1000)
    // revenues.forEach((revenue, i) => {
    //   // console.log( (i+1) ,(alert.alertDuration * 1000) + (alert.alertDelay * 1000))
    //   let duration = (i+1) * timer
    //   // console.log(duration,'dur')
    //   alertQueue.add({alert,user,revenue},{delay:duration});
    // })

  } catch (error) {
   console.log(error)
  }
}
module.exports = {
  sendAlert
}