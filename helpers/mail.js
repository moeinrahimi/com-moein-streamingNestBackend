"use strict";
const nodemailer = require("nodemailer");

// async..await is not allowed in global scope, must use a wrapper
async function main() {
  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing

  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: "127.0.0.1",
    port: 25,
    secure: false, // true for 465, false for other ports
    tls: {
      rejectUnauthorized: false
    },

    auth: {
      user: 'support@streaminglabs.ir', // generated ethereal user
      pass: 'moe2012@support' // generated ethereal password
    }
  });
  // verify connection configuration
// transporter.verify(function(error, success) {
//   if (error) {
//     console.log(error);
//   } else {
//     console.log("Server is ready to take our messages");
//   }
// });
  // return

  // send mail with defined transport object
//   let info = await transporter.sendMail({
//     from: '"Fred Foo 👻" <support@streaminglabs.ir>', // sender address
//     to: "imrrahimi@gmail.com", // list of receivers
//     subject: "Hello ✔", // Subject line
//     text: "Hello world?", // plain text body
//     html: "<b>Hello world?</b>" // html body
//   });

//   console.log("Message sent: %s", info.messageId);
//   // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

//   // Preview only available when sending through an Ethereal account
//   console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
//   // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
// }

main().catch(console.error);
