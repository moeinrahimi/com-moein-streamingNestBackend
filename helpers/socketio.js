const { io } = require('_/app')
const goalHelper = require("_/helpers/db/goalHelper")
io.on('connection', function (socket) {
  socket.on('NEW_CON', async(nanoid) => {
    let roomName = "ALERT" + nanoid
    socket.join(roomName)
    goalHelper.initGoal(roomName,nanoid)
  })
})

//emit to room io.to(currentRoom).emit('SCORE', scores)
module.exports.io=io