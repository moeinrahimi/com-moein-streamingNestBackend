module.exports = function (sequelize,DataTypes){
  return sequelize.define('alertBox',{
    id: { primaryKey: true, type: DataTypes.INTEGER,autoIncrement:true},
    media : {type:DataTypes.STRING},
    sound : {type:DataTypes.STRING},
    messageTemplate : {type:DataTypes.STRING},
    alertAnimationStart : {type:DataTypes.STRING},
    alertAnimationEnd: { type: DataTypes.STRING },
    soundVolume: { type: DataTypes.INTEGER },
    alertDuration: { type: DataTypes.INTEGER },
    backgroundColor: { type: DataTypes.STRING, defaultValue: '#fff' },
    alertDelay:{ type: DataTypes.INTEGER, defaultValue: 2 },
    layout:{ type: DataTypes.ENUM('above','banner','side'), defaultValue: 'above' },
    headerFontSize:{ type: DataTypes.INTEGER, defaultValue: 22 },
    headerTextAnimation:{ type: DataTypes.STRING, defaultValue: 'tada' },
    headerColor:{ type: DataTypes.STRING, defaultValue: '#000' },
    headerFontFamily:{ type: DataTypes.STRING, defaultValue: 'vazir' },
    headerWordSpacing:{ type: DataTypes.STRING, defaultValue: 'normal' },
    headerFontWeight:{ type: DataTypes.STRING, defaultValue: 'normal' },
    donorFontSize:{ type: DataTypes.INTEGER, defaultValue: 22 },
    donorTextAnimation:{ type: DataTypes.STRING, defaultValue: 'tada' },
    donorColor:{ type: DataTypes.STRING, defaultValue: '#000' },
    donorFontFamily:{ type: DataTypes.STRING, defaultValue: 'vazir' },
    donorWordSpacing:{ type: DataTypes.STRING, defaultValue: 'normal' },
    donorFontWeight:{ type: DataTypes.STRING, defaultValue: 'normal' },
  })
}

