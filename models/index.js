var Sequelize = require('sequelize');
var config = require('../config');
const Op = Sequelize.Op;
const operatorsAliases = {
  $eq: Op.eq,
  $ne: Op.ne,
  $gte: Op.gte,
  $gt: Op.gt,
  $lte: Op.lte,
  $lt: Op.lt,
  $not: Op.not,
  $in: Op.in,
  $notIn: Op.notIn,
  $is: Op.is,
  $like: Op.like,
  $notLike: Op.notLike,
  $iLike: Op.iLike,
  $notILike: Op.notILike,
  $regexp: Op.regexp,
  $notRegexp: Op.notRegexp,
  $iRegexp: Op.iRegexp,
  $notIRegexp: Op.notIRegexp,
  $between: Op.between,
  $notBetween: Op.notBetween,
  $overlap: Op.overlap,
  $contains: Op.contains,
  $contained: Op.contained,
  $adjacent: Op.adjacent,
  $strictLeft: Op.strictLeft,
  $strictRight: Op.strictRight,
  $noExtendRight: Op.noExtendRight,
  $noExtendLeft: Op.noExtendLeft,
  $and: Op.and,
  $or: Op.or,
  $any: Op.any,
  $all: Op.all,
  $values: Op.values,
  $col: Op.col,
};

var sequelize = new Sequelize(config.dbName, config.dbUser, config.dbPass, {
  host: config.dbHost,
  port: config.dbPort,
  dialect: 'mysql',
  charset: 'utf8',
  collate: 'utf8_general_ci',
  timezone: 'Asia/Tehran',
  logging: config.logging,
  operatorsAliases,
});
var db = {};
db.User = sequelize.import(__dirname + '/User');
db.AlertBox = sequelize.import(__dirname + '/AlertBox');
db.Setting = sequelize.import(__dirname + '/Setting');
db.StreamerRevenue = sequelize.import(__dirname + '/StreamerRevenue');
db.Goal = sequelize.import(__dirname + '/Goal');
db.Gallery = sequelize.import(__dirname + '/Gallery');
// db.Donor = sequelize.import(__dirname + "/Donor")
db.AlertBox.belongsTo(db.User);
db.Setting.belongsTo(db.User);
// db.User.hasOne(db.Donor)
db.StreamerRevenue.belongsTo(db.User);
db.Goal.belongsTo(db.User);
db.Gallery.belongsTo(db.User);
db.sequelize = sequelize;
db.Sequelize = Sequelize;
module.exports = db;
// CREATE USER 'streamhut'@'%' IDENTIFIED BY 'moe2012@';
// GRANT ALL PRIVILEGES ON *.* TO 'streamhut'@'localhost';




