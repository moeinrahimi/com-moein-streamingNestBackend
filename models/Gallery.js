module.exports = function (sequelize,DataTypes){
  return sequelize.define('gallery',{
    id: { primaryKey: true, type: DataTypes.INTEGER,autoIncrement:true},
    file: { type: DataTypes.STRING},
    path: { type: DataTypes.STRING},
    size: { type: DataTypes.STRING},
    type: { type: DataTypes.ENUM("image",'sound') },
    status: { type: DataTypes.ENUM("public",'private') },
  })
}

