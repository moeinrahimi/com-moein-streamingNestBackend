module.exports = function(sequelize, DataTypes) {
  const streamerRevenue =  sequelize.define('streamerRevenue', {
    id: { primaryKey: true, type: DataTypes.INTEGER, autoIncrement: true},
    fee: { type: DataTypes.INTEGER },
    authority: { type: DataTypes.STRING },
    refId: { type: DataTypes.STRING },
    transactionStatus: { type: DataTypes.STRING },
    name: { type: DataTypes.STRING },
    message: { type: DataTypes.STRING },
    sendStatus: { type: DataTypes.STRING },
    provider:{type:DataTypes.ENUM("zarinpal",'idpay')}

  })

  return streamerRevenue
}
