
ALTER TABLE alertBoxes add mediaType varchar(50) default null
ALTER TABLE alertBoxes add alertAnimationStart varchar(50) default null
ALTER TABLE alertBoxes add alertAnimationEnd varchar(50) default null
ALTER TABLE alertBoxes add soundVolume integer(50) default 100
ALTER TABLE alertBoxes add alertDuration integer(50) default 15
ALTER TABLE users add avatar varchar(50) default 'avatar.png'
ALTER TABLE users add twitchId varchar(20) default null
ALTER TABLE streamerRevenues add provider enum('zarinpal','idpay') not null


ALTER TABLE alertBoxes add headerFontSize integer(50) default 24;
ALTER TABLE alertBoxes add headerColor varchar(50) default "#000";
ALTER TABLE alertBoxes add headerTextAnimation varchar(50) default "tada";
ALTER TABLE alertBoxes add headerFontFamily varchar(50) default "vazir";
ALTER TABLE alertBoxes add headerWordSpacing varchar(50) default "normal";
ALTER TABLE alertBoxes add headerFontWeight varchar(50) default "normal";


ALTER TABLE alertBoxes add donorFontSize integer(50) default 24;

ALTER TABLE alertBoxes add donorColor varchar(50) default "#000";
ALTER TABLE alertBoxes add donorTextAnimation varchar(50) default "tada";
ALTER TABLE alertBoxes add donorFontFamily varchar(50) default "vazir";
ALTER TABLE alertBoxes add donorWordSpacing varchar(50) default "normal";
ALTER TABLE alertBoxes add donorFontWeight varchar(50) default "normal";





ALTER TABLE alertBoxes modify column headerColor varchar(50) default "#000";
ALTER TABLE alertBoxes modify column donorColor varchar(50) default "#000";





ALTER TABLE users add twitch varchar(50) default null;
ALTER TABLE users add aparat varchar(50) default null;
ALTER TABLE users add website varchar(50) default null;
ALTER TABLE users add telegram varchar(50) default null;
ALTER TABLE users add discord varchar(50) default null;
ALTER TABLE users add youtube varchar(50) default null;
ALTER TABLE users add instagram varchar(50) default null;
ALTER TABLE users add twitter varchar(50) default null;


ALTER TABLE `streamerrevenues` CHANGE `fee` `fee` INT(255) NULL DEFAULT NULL;