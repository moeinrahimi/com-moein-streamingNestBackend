
create database streamingnest;

CREATE USER 'moein' IDENTIFIED BY 'moe2012@';
GRANT USAGE ON *.* TO 'moein'@'%' IDENTIFIED BY 'moe2012@';
GRANT ALL privileges ON `streamingnest`.* TO 'moein'@%;
FLUSH PRIVILEGES;


-- restore db
-- cat ./models/stream.sql | docker exec -i a8bb2fc97e10 mysql -u root --password=moe2012@ streamingnest