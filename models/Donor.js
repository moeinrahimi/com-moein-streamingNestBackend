module.exports = function(sequelize, DataTypes) {
  const donor =  sequelize.define('donor', {
    id: { primaryKey: true, type: DataTypes.INTEGER, autoIncrement: true},
    name: { type: DataTypes.STRING },
    message: { type: DataTypes.STRING },
    status: { type: DataTypes.STRING },
    nanoid: { type: DataTypes.STRING },


  })

  return donor
}
