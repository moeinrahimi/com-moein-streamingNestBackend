module.exports = function (sequelize,DataTypes){
  return sequelize.define('goal',{
    id: { primaryKey: true, type: DataTypes.INTEGER,autoIncrement:true},
    progressColor: { type: DataTypes.STRING, defaultValue: '#f57507' },
    fontColor: { type: DataTypes.STRING, defaultValue: '#fff' },
    title: { type: DataTypes.STRING },
    initial: { type: DataTypes.INTEGER },
    amount: { type: DataTypes.INTEGER },
  })
}

