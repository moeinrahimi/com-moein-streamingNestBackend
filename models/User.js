module.exports = function (sequelize,DataTypes){
  return sequelize.define('user', {
    id: { primaryKey: true, type: DataTypes.INTEGER,autoIncrement:true},
    userName : {type:DataTypes.STRING,unique:true},
    fname : {type:DataTypes.STRING},
    avatar: { type: DataTypes.STRING },
    nationalId: { type: DataTypes.STRING(11) },
    mobile: { type: DataTypes.STRING(17) },
    email: { type: DataTypes.STRING,unique:true },
    password: { type: DataTypes.STRING(60) },
    nanoid:{type: DataTypes.STRING},
    twitchId:{type: DataTypes.STRING},
    twitch:{type: DataTypes.STRING},
    aparat:{type: DataTypes.STRING},
    youtube:{type: DataTypes.STRING},
    discord:{type: DataTypes.STRING},
    telegram:{type: DataTypes.STRING},
    instagram:{type: DataTypes.STRING},
    website:{type: DataTypes.STRING},
    twitter:{type: DataTypes.STRING},
  })
}

