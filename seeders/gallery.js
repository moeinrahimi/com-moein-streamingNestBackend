//TODO: gallery seeder
const fs = require('fs');
const db = require('_/models')
let files = fs.readdirSync("./public/uploads/media/public")
files.forEach(file => {
  let stat = fs.statSync('./public/uploads/media/public/'+file)
  db.Gallery.findOrCreate({
    where: {
      file: file,
      path: "/uploads/media/public/" + file,
      size:stat.size,
      status: 'public',
      type:'image'
    }
  })
})

files = fs.readdirSync("./public/uploads/sounds/public")
files.forEach(file => {
  let stat = fs.statSync('./public/uploads/sounds/public/'+file)
  db.Gallery.findOrCreate({
    where: {
      file: file,
      path: "/uploads/sounds/public/" + file,
      size:stat.size,
      status: 'public',
      type:'sound'
    }
  })
})